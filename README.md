# Conan

## Pipeline to create Conan package for Linux

Conan 2.12.1 for:
* Linux 32 bits: [conan-2.12.1-linux-i686.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/conan/-/jobs/artifacts/2.12.1/download?job=linux-i686)
* Linux 64 bits: [conan-2.12.1-linux-x86_64.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/git/-/jobs/artifacts/2.12.1/download?job=linux-x86_64)

## Python script to create Conan package for Windows 32 bits

The script `build-windows-i386.py` allows to build Conan package for Windows 7 32 bits.

Prerequisites on path on the build host:
* Python
* Git

To launch build:
```
python build-windows-i386.py
```

The build deliveries the archive `build\conan-2.12.1-windows-i386.zip`

Tested with python 3.6 on Windows 7 32 bits. Warning, it is mandatory to use installed version python on Windows 7 to include all dll in package with PytInstaller.
