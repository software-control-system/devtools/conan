import os
import subprocess
import urllib.request
import shutil

CONAN_VERSION = '2.12.1'

CONAN_ROOT = os.path.dirname(os.path.abspath(__file__))

# Remove and recreate build directory
BUILD_DIR=os.path.join(CONAN_ROOT, 'build')
os.system(f"rmdir /s /q {BUILD_DIR} > nul 2>&1")
os.makedirs(BUILD_DIR)
os.chdir(BUILD_DIR)

# Build Conan with PyInstaller
conan_dir = os.path.join(BUILD_DIR, 'conan_src')
subprocess.check_call(['git', 'clone', 'https://github.com/conan-io/conan', conan_dir])
subprocess.check_call(['git', 'checkout', CONAN_VERSION], cwd=conan_dir)
subprocess.check_call(['pip', 'install', '-e', conan_dir])
subprocess.check_call(['python', 'pyinstaller.py'], cwd=conan_dir)
conan_exe = os.path.join(conan_dir, 'pyinstaller', 'dist', 'conan', 'conan.exe')
subprocess.check_call([conan_exe, '--version'])

# Add Sectigo CA certificates to Conan CA certificates
cacert_sectigo=os.path.join(CONAN_ROOT, 'conan_synchrotron-soleil_fr_interm.cer')
cacert=os.path.join(conan_dir, 'pyinstaller', 'dist', 'conan', 'certifi', 'cacert.pem')
with open(cacert_sectigo, 'r') as f1:
    with open(cacert, 'a') as f2:
        f2.write(f1.read())

# Make archive
shutil.make_archive(os.path.join(BUILD_DIR, f'conan-{CONAN_VERSION}-windows-i386'), 'zip',
                    os.path.join(conan_dir, 'pyinstaller', 'dist', 'conan')) 
